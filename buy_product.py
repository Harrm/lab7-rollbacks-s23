import psycopg2

# DB commands:
# CREATE TABLE Inventory (username TEXT, product TEXT, amount INT CHECK(amount >= 0));
# CREATE TABLE InventoryCount (username TEXT, total_amount INT CHECK(total_amount >= 0 AND total_amount <= 100));
# create unique index username_product on inventory (username, product);
# create unique index username on inventorycount (username);

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
buy_add_inventory = """INSERT INTO Inventory (username, product, amount) 
VALUES (%(username)s, %(product)s, %(amount)s)
ON CONFLICT (username, product) DO UPDATE 
  SET amount = Inventory.amount + %(amount)s;
"""
buy_increase_total = """
INSERT INTO InventoryCount (username, total_amount) 
VALUES (%(username)s, %(amount)s)
ON CONFLICT (username) DO UPDATE 
  SET total_amount = InventoryCount.total_amount + %(amount)s;
"""

buy_transaction = f"""
BEGIN;
{buy_decrease_balance};
{buy_decrease_stock};
{buy_add_inventory};
{buy_increase_total};
COMMIT;
"""

def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="hello",
        host="0.0.0.0",
        port=5432
    )  # TODO add your values here


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_transaction, obj)
            except psycopg2.errors.CheckViolation as e:
                raise Exception(e.pgerror)

buy_product('Bob', 'foo', 99)